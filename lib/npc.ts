export class StockNpc {
    private _rating: number;
    private _specilizations: Array<string>;
    constructor(rating: number, specializations: Array<string> = []) {
        this._rating = rating;
        this._specilizations = specializations;
    }

    get rating(): number {
        return this._rating;
    }

    get maxHealth(): number {
        return this._rating;
    }

    poolFor(skill: string): number {
        return this._rating * 2 + (this._specilizations.indexOf(skill) == -1? 0 : 5)
    }

}