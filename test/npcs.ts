import 'jasmine'
import { StockNpc } from '../lib/npc'

describe("Stock NPCS", () => {
    let npc = new StockNpc(1);
    it("Has a rating", () => {
        expect(npc.rating).toBe(1)
    });
    it("Has health equal to it's rating", () => {
        expect(npc.maxHealth).toBe(1)
    })
    it("Has Dicepools", () => {
        expect(npc.poolFor("Brawl")).toBe(2)
    })

    describe("have specializations", () => {
        let npc = new StockNpc(2, ["Intimidate", "Dominate"]);

        it("has normal rating for non-specs", () => {
            expect(npc.poolFor("Melee")).toBe(4)
        });
        it("Gets a bonus for a specialization", ()=>{
            expect(npc.poolFor("Intimidate")).toBe(9)
        });
    });

})